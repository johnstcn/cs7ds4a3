import React from 'react';
import PropTypes from 'prop-types';
import * as d3 from 'd3';
import * as d3Chromatic from 'd3-scale-chromatic';
import * as ReactFauxDOM from 'react-faux-dom';

class LineChart extends React.Component {
    render() {
        const {data, width, height, margin, maxPos, minVel, maxVel, laneChanges, laneID} = this.props;
        const h = height - 2 * margin;
        const w = width - 2 * margin;

        // create a scale function for our x-axis
        const x = d3.scaleTime()
            .domain(d3.extent(data, d => d.Global_Time))
            .range([margin, w]);

        // create a scale function for our y axis
        const y = d3.scaleLinear()
            .domain([0, maxPos])
            .range([h, margin]);

        // draw an x-axis
        const xAxis = function (g) {
            g.attr('transform', `translate(0, ${height - (2 * margin)})`) // TODO: why subtract twice the margin?
                .call(d3.axisBottom(x)
                    .ticks(width / 100)
                );
        }

        // draw a y-axis
        const yAxis = function (g) {
            g.attr('transform', `translate(${margin},0)`)
                .call(d3.axisLeft(y));
        }

        // draw our line, time on the x-axis, position on the y-axis
        const line = d3.line()
            .x(d => x(d.Global_Time))
            .y(d => y(d.v_Pos))
            .curve(d3.curveBasis);

        // slice the data by vehicle ID
        const positionByVehicle = d3.nest()
            .key(function (row) {
                return row.Vehicle_ID;
            })
            .entries(data)

        // add a separate linear scale for a vehicle's position along the track to map to the gradient offset %.
        // could repurpose another scale and divide, but this is cleaner.
        const posScale = d3.scaleLinear()
            .domain([0, maxPos])
            .range([0, 100])
        // add a colour scale for velocity
        const colourScale = d3.scaleSequential()
            .interpolator(d3Chromatic.interpolateViridis)
            .domain([0, maxVel * 0.75])
            .clamp(true)

        // because react and d3 both want control over the DOM, we create a faux DOM to give
        // d3 something to mess around with while not disturbing react
        // More reading: https://oli.me.uk/d3-within-react-the-right-way/
        const node = ReactFauxDOM.createElement('svg');

        // create an SVG node in our fake dom
        const svg = d3.select(node)
            .attr('width', width)
            .attr('height', height)
        // .attr('overflow', 'auto')

        // add an X-axis
        svg.append('g')
            .call(xAxis);

        // Add a label for the x-axis
        svg.append('text')
            .attr('transform', `translate(${w / 2}, ${h + margin})`)
            .text('Time (s)')

        // add a y-axis
        svg.append('g')
            .call(yAxis);

        // add a label for the y-axis
        svg.append('text')
            .attr('transform', 'rotate(-90)')
            .attr('x', 0 - (height / 2))
            .attr('y', (margin * 0.25))
            .text('Distance (km)')

        // add a linear gradient for each vehicle
        svg.selectAll('linearGradient')
            .data(positionByVehicle)
            .enter()
            .append('linearGradient')
            .attr('id', function (d) {
                return `gradient_vehicle_${d.key}`
            })
            .attr('x1', "0%")
            .attr('y1', "100%")
            .attr('x2', "0%")
            .attr('y2', "0%")
            .selectAll('stop')
            .data(d => d.values)
            .join('stop')
            .attr('offset', d => `${posScale(d.v_Pos)}%`)
            .attr('stop-color', d => colourScale(d.v_Vel))

        // scale stroke width to the amount of data on screen
        const strokeWidth = Math.min(3, Math.max(1, (w / positionByVehicle.length) * 0.1));

        // let d3 do its thing
        svg.append('g')
            .attr('fill', 'none')
            .attr('stroke-width', strokeWidth)
            .selectAll('path')
            .data(positionByVehicle)
            .join('path')
            .attr('stroke', function (d) {
                return `url(#gradient_vehicle_${d.key})`
            })
            .attr('d', function (d) {
                return line(d.values);
            })

        // add a separate y-scale for the legend
        const legendHeight = h - margin;
        const legendY = d3.scaleLinear()
            .domain([minVel, maxVel])
            .range([legendHeight, 0])
        const legendStops = legendY.ticks(10)
            .map(function (d) {
                return {
                    vel: d,
                    y: legendY(d) + margin,
                    offset: `${(d / maxVel * 100).toFixed(2)}%`,
                    color: `${colourScale(d)}`
                };
            });

        // add a separate gradient for the legend
        svg.append('linearGradient')
            .attr('id', 'gradient_legend')
            .attr('x1', '0%')
            .attr('y1', '100%')
            .attr('x2', '0%')
            .attr('y2', '0%')
            .selectAll('stop')
            .data(legendStops)
            .enter()
            .append('stop')
            .attr('offset', d => d.offset)
            .attr('stop-color', d => d.color)

        // add a rect for the legend
        const legend = svg.append('g');
        legend.append('rect')
            .attr('width', `${margin / 5}`)
            .attr('height', `${legendHeight}`)
            .style('fill', 'url(#gradient_legend)')
            .attr('x', `${w + margin / 2}`)
            .attr('y', `${margin}`)

        // add labels for the legend
        legend.selectAll('text')
            .data(legendStops)
            .enter()
            .append('text')
            .text(d => d.vel)
            .attr('x', `${w + (margin * 0.8)}`)
            .attr('y', d => (d.y))
            .style('font-size', '12px')

        // add ticks for the legend
        const tickX = w + (margin / 2)
        const tickWidth = 20;
        legend.selectAll('path')
            .data(legendStops)
            .enter()
            .append('line')
            .attr('x1', `${tickX}`)
            .attr('x2', `${tickX + tickWidth}`)
            .attr('y1', d => d.y)
            .attr('y2', d => d.y)
            .style('stroke', 'black')

        // add a label for the legend
        svg.append('text')
            .attr('transform', 'rotate(90)')
            .attr('x', (h / 2))
            .attr('y', (-width + (margin * 0.8)))
            .text('Velocity (km/h)')

        // indicate lane changes with circle marks
        svg.append('g')
            .selectAll('circle')
            .data(laneChanges.filter(function (d) {
                return (d.laneFrom === laneID) || (d.laneTo === laneID);
            }))
            .enter()
            .append('circle')
            .attr('cx', function (d) {
                // fudging the lane changes slightly; the right way would be to put the lane change
                // as an intermediate point and continue the line
                if (d.laneFrom === laneID) {
                    return x(d.timeFrom);
                }
                if (d.laneTo === laneID) {
                    return x(d.timeTo);
                }
            })
            .attr('cy', function (d) {
                // fudging the lane changes slightly; the right way would be to put the lane change
                // as an intermediate point and continue the line
                if (d.laneFrom === laneID) {
                    return y(d.posFrom);
                }
                if (d.laneTo === laneID) {
                    return y(d.posTo);
                }
                return y(d.pos);
            })
            .attr('r', strokeWidth)
            .style('fill', 'black')

        // and finally let react do its thing!
        return node.toReact()
    }
}

LineChart.propTypes = {
    width: PropTypes.number,
    height: PropTypes.number,
    margin: PropTypes.number,
    data: PropTypes.array,
    start: PropTypes.instanceOf(Date),
    end: PropTypes.instanceOf(Date),
    maxPos: PropTypes.number,
    minVel: PropTypes.number,
    maxVel: PropTypes.number,
    laneChanges: PropTypes.array,
    laneID: PropTypes.number,
}

export default LineChart;