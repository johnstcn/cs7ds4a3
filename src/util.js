export function arrMax(arr) {
    let len = arr.length;
    let max = -Infinity;
    while (len--) {
        max = arr[len] > max ? arr[len]: max;
    }
    return max;
}

export function arrMin(arr) {
    let len = arr.length;
    let min = Infinity;
    while (len--) {
        min = arr[len] < min ? arr[len]: min;
    }
    return min;
}