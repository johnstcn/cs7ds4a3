import React from 'react';
import * as d3 from 'd3';
import './App.css';
import LineChart from './LineChart.js';
import {arrMin, arrMax} from './util.js'

var FT_TO_KM = 0.0003048;
var FTS_TO_KMH = 1.09728;

class App extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            data: [],
            width: 0,
            height: 0,
            startTs: 0,
            endTs: 0,
            minTs: 0,
            maxTs: 0,
            maxPos: 0,
            minVel: 0,
            maxVel: 0,
            laneID: 1,
        };
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
        this.handleBtnTimeBackClick = this.handleBtnTimeBackClick.bind(this);
        this.handleBtnTimeFwdClick = this.handleBtnTimeFwdClick.bind(this);
        this.handleBtnZoomInClick = this.handleBtnZoomInClick.bind(this);
        this.handleBtnZoomOutClick = this.handleBtnZoomOutClick.bind(this);
        this.handleSelectLaneId = this.handleSelectLaneId.bind(this);
    }

    updateWindowDimensions() {
        this.setState({
            width: window.innerWidth,
            height: window.innerHeight,
        });
    }

    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions)
        d3.csv('/munged.csv', function (row) {
            const ts = parseInt(row.Global_Time, 10);
            return {
                Global_Time: new Date(ts),
                Vehicle_ID: parseInt(row.Vehicle_ID, 10),
                Lane_ID: parseInt(row.Lane_ID, 10),
                v_Vel: parseInt(row.v_Vel, 10) * FTS_TO_KMH,
                v_Pos: parseInt(row.v_Pos, 10) * FT_TO_KM,
            }
        }).then((data) => {
            const maxPos = arrMax(data.map(function(d) { return d.v_Pos; }));
            const minTs = arrMin(data.map(function (row) {
                return row.Global_Time.valueOf();
            }));
            const maxTs = arrMax(data.map(function (row) {
                return row.Global_Time.valueOf();
            }));
            const minVel = arrMin(data.map(function(row) {
                return row.v_Vel;
            }));
            const maxVel = arrMax(data.map(function(row) {
                return row.v_Vel;
            }));
            // calculate lane changes. TOOD: perform this in pre-processing stage.
            let lastLaneSeen = new Map(data.map(function(row) { return [row.Vehicle_ID, null]; }));
            const laneChanges = [];
            data.forEach(function(row) {
                const vid = row.Vehicle_ID;
                const currLane = row.Lane_ID;
                const currPos = row.v_Pos;
                const currTime = row.Global_Time;
                const lastSeen = lastLaneSeen.get(vid);
                const o = {lastLane: currLane, lastPos: currPos, lastTime: currTime};
                if (lastSeen === null) {
                    // first one
                    lastLaneSeen.set(vid, o);
                    return;
                }
                const {lastLane, lastPos, lastTime} = lastSeen;
                if (lastLane !== currLane) {
                    laneChanges.push({vid: vid, timeFrom: lastTime, timeTo: currTime, posFrom: lastPos, posTo: currPos, laneFrom: lastLane, laneTo: currLane});
                }
                lastLaneSeen.set(vid, o);
            })
            this.setState({
                loading: false,
                data: data,
                laneChanges: laneChanges,
                minTs: minTs,
                maxTs: maxTs,
                startTs: minTs,
                endTs: maxTs,
                maxPos: maxPos,
                minVel: minVel,
                maxVel: maxVel,
            }, function () {
            });
        });
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    handleBtnTimeBackClick() {
        console.log("handleBtnTimeBackClick");
        const {minTs, startTs, endTs} = this.state;
        const windowSize = endTs - startTs;
        const newStartTs = Math.max(minTs, startTs - (windowSize / 2));
        const newEndTs = Math.max(minTs, endTs - (windowSize / 2));
        this.setState({
            startTs: newStartTs,
            endTs: newEndTs,
        });
    }

    handleBtnTimeFwdClick() {
        console.log("handleBtnTimeFwdClick");
        const {startTs, endTs, maxTs} = this.state;
        const windowSize = endTs - startTs;
        const newStartTs = Math.min(maxTs, startTs + (windowSize / 2));
        const newEndTs = Math.min(maxTs, endTs + (windowSize / 2));
        this.setState({
            startTs: newStartTs,
            endTs: newEndTs,
        });
    }

    handleBtnZoomInClick() {
        console.log("handleBtnZoomInClick");
        const {startTs, endTs} = this.state;
        const windowSize = endTs - startTs;
        const midpoint = startTs + (windowSize / 2);
        const newWindowSize = windowSize / 2;
        const newStartTs = midpoint - (newWindowSize / 2);
        const newEndTs = midpoint + (newWindowSize / 2);
        this.setState({
            startTs: newStartTs,
            endTs: newEndTs,
        });
    }

    handleBtnZoomOutClick() {
        console.log("handleBtnZoomOutClick");
        const {startTs, endTs, minTs, maxTs} = this.state;
        const windowSize = endTs - startTs;
        const midpoint = startTs + (windowSize / 2);
        const newWindowSize = windowSize * 2;
        const newStart = Math.max(minTs, midpoint - (newWindowSize / 2));
        const newEnd = Math.min(maxTs, midpoint + (newWindowSize / 2));
        this.setState({
            startTs: newStart,
            endTs: newEnd,
        });
    }

    handleSelectLaneId(event) {
        console.log(`handleSelectLaneId event:${event.target.value}`);
        const laneID = parseInt(event.target.value, 10);
        this.setState({
            laneID: laneID,
        });
    }

    render() {
        const {loading, width, height, startTs, endTs, minTs, maxTs, maxPos, minVel, maxVel, laneID, data, laneChanges} = this.state;
        if (data.length === 0) {
            return null;
        }
        const startTime = new Date(startTs);
        const endTime = new Date(endTs);
        let filteredData = data.filter(function (row) {
            const isWithinTime = (row.Global_Time >= startTime && row.Global_Time <= endTime)
            const isSelectedLane = row.Lane_ID === laneID;
            return isWithinTime && isSelectedLane;
        });

        const backEnabled = startTs > minTs;
        const fwdEnabled = endTs < maxTs;
        const zoomInEnabled = (endTs - startTs) > 10000;
        const zoomOutEnabled = (startTs > minTs) || (endTs < maxTs);

        const chart = loading? null : (
            <LineChart
                width={width * 0.8}
                height={height * 0.7}
                margin={80}
                data={filteredData}
                start={startTime}
                end={endTime}
                maxPos={maxPos}
                minVel={minVel}
                maxVel={maxVel}
                laneID={laneID}
                laneChanges={laneChanges}
            />
        );

        const laneOptions = [...new Set(data.map(function(row) { return row.Lane_ID }))].sort().map(function(laneID) {
            return (
                <option key={`opt_lane_${laneID}`} value={`${laneID}`}>Lane {laneID}</option>
            );
        })

        return (
            <div className="App">
                <div className="App-header">
                    <h3>NGSIM US-101 Highway Data Visualization</h3>
                </div>
                <div className={"doc"}>
                    <p>
                        This is a graph of vehicle position over time along the <a href={"https://www.google.com/maps/place/Hollywood+Fwy,+Los+Angeles,+CA,+USA/@34.1377742,-118.364421,17.5z/data=!4m5!3m4!1s0x80c2bfbc1170e69b:0xd2f0eb183d896813!8m2!3d34.1292295!4d-118.3473138"}>NGSIM study area of US-101</a>.<br />
                        Each line represents the trajectory of an individual vehicle in the selected lane. <br />
                        Time (X axis) is shown in your local timezone; Position is measured on the Y-Axis, and velocity is shown by the trajectory line colouring. <br />
                        Lane change events are indicated with a dot. <br/>
                        More information on NGSIM is available at <a href={"http://ops.fhwa.dot.gov/trafficanalysistools/ngsim.htm"}>fhwa.dot.gov</a>.
                    </p>
                </div>
                <div id="chart">
                    {chart}
                </div>
                <div id="control">
                    <label htmlFor={"select_laneid"}>Select Lane:</label>
                    <select id={"select_laneid"} onChange={this.handleSelectLaneId}>
                        {laneOptions}
                    </select>
                    <button id={"btn_time_back"} onClick={this.handleBtnTimeBackClick}
                            disabled={!backEnabled}>&larr; Back
                    </button>
                    <button id={"btn_zoom_out"} onClick={this.handleBtnZoomOutClick}
                            disabled={!zoomOutEnabled}>&#x2296; Zoom Out
                    </button>
                    <button id={"btn_zoom_in"} onClick={this.handleBtnZoomInClick} disabled={!zoomInEnabled}>Zoom
                        In &#x2295;</button>
                    <button id={"btn_time_fwd"} onClick={this.handleBtnTimeFwdClick}
                            disabled={!fwdEnabled}>Forward &rarr;</button>
                </div>
                <div id={"footer"}>
                    Author: Cian Johnston <a href={"mailto:nope"}>&lt;johnstc AT tcd DOT ie&gt;</a>
                </div>
            </div>
        );
    }
}

export default App;
    