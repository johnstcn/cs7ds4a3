#!/usr/bin/env python3

import argparse
import csv
import json
import sys

from collections import defaultdict

parser = argparse.ArgumentParser()
parser.add_argument('input', help='csv file to read')
parser.add_argument('--sample', type=int, default=10, help='take one in every n rows')
args = parser.parse_args()

idx = {
    'Vehicle_ID': 0,
    'Global_Time': 3,
    'v_Vel': 11,
    'Lane_ID': 13,
}

with open(args.input) as f:
    reader = csv.reader(f)
    rows = [row for row in reader]
    header_in = rows[0]
    data = rows[1:]

header_out = ["Global_Time", "Vehicle_ID", "Lane_ID", "v_Vel", "v_Pos"]
vehicle_ids = set(map(lambda r : r[idx['Vehicle_ID']], data))

# add v_Pos to the file
positions = {}
data_by_vid = defaultdict(list)
for row in data:
    vid = row[0]
    vel = float(row[11])
    last_pos = positions.get(vid, 0.0)
    data_by_vid[vid].append([row[3], row[0], row[13], row[11], '%0.2f' % last_pos])
    last_pos += vel / args.sample
    positions[vid] = last_pos

writer = csv.writer(sys.stdout)
writer.writerow(header_out)
for key in data_by_vid.keys():
    for (i, row) in enumerate(data_by_vid[key]):
        if i % args.sample != 0:
            continue
        writer.writerow(row)
