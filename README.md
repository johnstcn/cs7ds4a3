# CS7DS4 Assignment 3
*NGSIM US-101 Visualization*

This project provides an in-browser interactive view of NGSIM dataset. 
It has been tested on the I-80 dataset, but should work on other NGSIM datsets as well.

## Background
To support development of driver models to reliability model human driver behaviour, 
researchers for the Next Generation Simulation (NGSIM) (1) program of the U.S. Department
 of Transportation (DoT) Federal Highway Administration (FHA) collected detailed traffic 
information at a number of locations in the U.S. This data is freely accessible at (2).

## Motivation
Most visualizations of the NGSIM dataset are in the form of a static position/time graph. For example, Li
et al. (3) provide a number of graphs of traffic shockwave formation in the US-101 NGSIM dataset
 ([link to hi-res version](https://ars.els-cdn.com/content/image/1-s2.0-S0968090X19312987-gr1_lrg.jpg)).
However, even with the high resolution, it is difficult to investigate individual vehicle trajectories. 
The aim of this project is to enable both a high-level exploration of the dataset, and a more in-depth
exploration at the same time.

## Data

A copy of the original data used is available in the project root directory (`ngsim-us101-0750-0805.csv`).
For performance reasons, some preprocessing steps are performed on the data before being used by the application.
These steps are performed by a Python script (`munge.py`). The final resulting data used by the application
is located in the `./public` folder.

Two versions of the preprocessed data are provided:
  - `munged.csv` is the entire preprocessed version of the dataset (100,000 rows approx.)
  - `munged.small.csv` consists of the first 10,000 rows of the entire preprocessed dataset. 
  This is mainly included for development purposes.

## Setup

This project can be run in one of two ways:
 - Directly, using `node` in your host system
 - In Docker (does not require Node or NPM installed) (recommended)
 
### Running Locally (with NodeJS):

If you are unable to run in docker, this is the preferred method.
1. Clone the git repository and enter the folder: `cd ./cs7ds4a3/`
2. Run `npm install`. This may take a while.
3. Run `npm start`. Your browser should open up on `http://localhost:3000`.

### Running in a Docker image

If you have Docker installed, this is the preferred method, as you do not need to have NodeJS or NPM installed.

*Note:* Ensure you can run the following command:
```shell script
docker run hello-world
``` 
If this fails, please consult
the [Docker documentation](https://docs.docker.com/) for troubleshooting steps for your respective platform.

1. Clone the git repository and enter the folder: `cd ./cs7ds4a3/`
2. Run the following command to build the Docker image for the project (this may take a while): `docker build . -t cs7ds4a3:latest`
3. Run the docker image with the following command, binding port 3000 on your local machine to port 80 in the container: 
`docker run -p 3000:80 --rm cs7ds4a3:latest`.  
4. Then, open `http://localhost:3000` in your browser. 

*Notes:*
 - If port 3000 is unavailable, simply choose another port and adjust the command in step 3 accordingly. 
 - Press `Control-C` to stop the application.
 
## References

1. NGSIM Homepage, https://ops.fhwa.dot.gov/trafficanalysistools/ngsim.htm (Accessed Apr 19, 2020)
2. U.S. Department of Transportation Federal Highway Administration. (2016). Next Generation Simulation (NGSIM) Vehicle Trajectories and Supporting Data. [Dataset]. Provided by ITS DataHub through Data.transportation.gov. Accessed 2020-04-19 from http://doi.org/10.21949/1504477
3. Li, Li, Rui Jiang, Zhengbing He, Xiqun Michael Chen, and Xuesong Zhou. "Trajectory data-based traffic flow studies: A revisit." Transportation Research Part C: Emerging Technologies 114 (2020): 225-240.
